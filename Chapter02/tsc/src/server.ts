import Koa from 'koa';
import {Context} from 'koa';

const app = new Koa();


// register logger

app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

//register  x-response-time

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// middleware to send response
app.use(async (ctx: Context)  => {
  ctx.body = 'Hello World';
});

app.listen(1234,() => {
  console.log('Server is running on port 1234');
})