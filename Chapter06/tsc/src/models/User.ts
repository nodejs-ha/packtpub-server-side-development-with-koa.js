import mongoose from 'mongoose';
import mongooseUniqueValidator  from 'mongoose-unique-validator'

import { IUser } from '../shared/IUser'

interface IUserModel extends IUser, mongoose.Document { }

const schema = new mongoose.Schema({
 fullName: {
  type: String,
  required: true
 },
 email: {
  unique: true,
  type: String,
  required: true
 },
 password: {
  type: String,
  required: true
 },
 createdAt: { type: Date, default: Date.now },
 updatedAt: { type: Date, default: Date.now }
});

schema.plugin(mongooseUniqueValidator);

// const User = mongoose.model('User', schema);
const User = mongoose.model<IUserModel>("User", schema);
export { User } 