// ./middleware/guest.js

import { BaseContext } from 'koa';
import { NextFunction } from 'connect';



function guest() {
  return async (ctx :BaseContext, next :NextFunction) => {
    const { user } = ctx.session;
    if (user) ctx.redirect('/');
    else await next();
  };
};

export {guest }