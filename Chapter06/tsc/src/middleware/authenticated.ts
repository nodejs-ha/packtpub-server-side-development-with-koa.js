import { BaseContext } from 'koa';
import { NextFunction } from 'connect';


function authenticated() {
  return async (ctx :BaseContext, next :NextFunction) => {
    const { user } = ctx.session;
    if (user) await next();
    else ctx.redirect('/auth');
  };
};

export { authenticated }