// ./middleware/method-override.js
import { BaseContext } from 'koa';
import { NextFunction } from 'connect';


function methodOverride() {
  return async (ctx :BaseContext, next :NextFunction) => {
    const { method } = ctx.request.body;
    if (method) ctx.method = method;
    await next();
  };
};

export { methodOverride }