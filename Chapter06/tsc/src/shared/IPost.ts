import mongoose from 'mongoose';

interface IPost {
    title: string;
    content: string;
    image: string;
    // author: {
    //      type: mongoose.Schema.Types.ObjectId,
    //      ref: 'User'
    //   },
    createdAt: Date;
    updatedAt: Date;

}
export {IPost}