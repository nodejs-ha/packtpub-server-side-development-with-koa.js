import Koa from 'koa';
import logger from 'koa-logger';
import  bodyParser  from 'koa-body';

import mongoose from 'mongoose';

import {router} from './middleware/router';
import {validatorFunc} from  './middleware/validator'
// import { Router } from 'express';


const app = new Koa();



mongoose.connect(
  'mongodb://localhost:27017/koa-contact',
  { useNewUrlParser: true }
);

const db = mongoose.connection;
db.on('error', error => {
  throw new Error(`error connecting to db: ${error}`);
});
db.once('open', () => console.log('database connected'));

app.use(logger());

app.use(bodyParser());

app.use(validatorFunc());

app.use(router.routes());
app.use(router.allowedMethods());

const port = process.env.PORT || 3000;
app.listen(port, () =>
  console.log(`Server running on http://localhost:${port}`)
);