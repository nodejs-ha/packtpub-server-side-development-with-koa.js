import Koa from 'koa';
import {KoaRouter} from './server_router'

 
//TODO: make KoaRouter a class, with koajs router instance its implementation

// define which data members to expose in construcor
export class ServerRun {
    private port: number;
    private app: Koa;
    private router :any;
    // private router: KoaRouter;
    private count: number;
 
    // define and initialze data members here...
    public constructor(port: number) {
        this.port = port;
        this.app = new Koa();
        this.router = KoaRouter;
        this.count = 0;
    }
 
    public start() {
      
        this.app.use(this.router.routes());
        this.app.use(this.router.allowedMethods());
 
        const server = this.app.listen(this.port, () => {
            console.log(`Server process: ${process.pid} listen on port ${this.port}`);
        });
 
        this.app.on("error", (e) => console.log(`SEVERE ERROR: ${e.message}`) );
    }

    public incrementCount() {
        this.count++;
    }
}

