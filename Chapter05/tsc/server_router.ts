import {BaseContext} from "koa";
import Router  from 'koa-router';
import { NextFunction } from "connect";
import { RouterOptions } from "express";

// moved count from server to router to avoid ciruclar reference.
let count :number  = 0;

const  KoaRouter = new Router();


     
KoaRouter.get("/", async (ctx :BaseContext, next :NextFunction) => {
    //FIX-ME: how do you get server counter instance
    count++;
    ctx.body = `This is server process : ${process.pid} and count: ${count}`;
    ctx.status = 200;
    await next();
});

KoaRouter.get("/stop", async (ctx :BaseContext, next :NextFunction) => {
    console.log(`Stopping server process : ${process.pid}`);
    process.exit(1);
    await next();
});





export  {KoaRouter};
