
// Run server for debug in single process without cluster
import {ServerRun} from "./server_run";
 
const app = new ServerRun(3002);
app.start();