import {BaseContext} from "koa";
import {Contact}  from '../models/Contact';

export class ContactController {
  constructor() {}

  public async index(ctx :BaseContext) {
    const contacts = await Contact.find();
    ctx.body = {
      status: 'success',
      data: contacts
    };
  }

 public  async store(ctx :BaseContext) {
    const { body } = ctx.body;
    let contact = new Contact(body);
    contact = await contact.save();
    ctx.body = {
      status: 'success',
      data: contact
    };
  }

  public async show(ctx :BaseContext) {
    const { id } = ctx.params;
    const contact = await Contact.findById(id);
    ctx.body = {
      status: 'success',
      data: contact
    };
  }
 
  public async update(ctx :BaseContext) {
    const { id } = ctx.params;
    const { body } = ctx.body;
    await Contact.findByIdAndUpdate(id, body);
    const contact = await Contact.findById(id);
    ctx.body = {
      status: 'success',
      message: 'contact successfully updated',
      data: contact
    };
  }

  public async destroy(ctx :BaseContext) {
    const { id } = ctx.params;
    await Contact.findByIdAndDelete(id);
    ctx.body = {
      status: 'success',
      message: 'contact successfully deleted'
    };
  }
};