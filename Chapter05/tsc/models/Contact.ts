// import { Document, Schema, Model, model} from "mongoose";
import * as mongoose from 'mongoose';

const contactSchema = new mongoose.Schema({
   name: {
       type: String,
       required: true
   },
   company: String,
   position: String,
   address: String,
   phoneNumber: String,
   createdAt: { type: Date, default: Date.now },
   updatedAt: { type: Date, default: Date.now }
});

const Contact = mongoose.model('Contact', contactSchema);
export {Contact};